from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoTaskForm


def todo_list(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list_list,
    }
    return render(request, "todos/todo_list.html", context)


def todo_items(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_list_detail,
    }
    return render(request, "todos/todo_list_detail.html", context)


def create_view(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            create = form.save()
            return redirect("todo_list_detail", id=create.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create_todos.html", context)


def todo_update_view(request, id):
    todosform = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todos_form = TodoListForm(request.POST, instance=todosform)
        if todos_form.is_valid():
            todos_form.save()
            return redirect("todo_list_detail", id=id)
    else:
        todos_form = TodoListForm(instance=todosform)
    context = {
        "todos_form": todos_form,
    }
    return render(request, "todos/update_todos.html", context)


def todo_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")
    context = {
        "delete_view": model_instance,
    }
    return render(request, "todos/delete_todos.html", context)


def task_view(request):
    if request.method == "POST":
        task_form = TodoTaskForm(request.POST)
        if task_form.is_valid():
            task = task_form.save()
            return redirect("todo_list_detail", task.list.id)
    else:
        task_form = TodoTaskForm()

    context = {
        "task_form": task_form,
    }
    return render(request, "todos/todos_task.html", context)


def task_update(request, id):
    taskupdate = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        todos_form = TodoTaskForm(request.POST, instance=taskupdate)
        if todos_form.is_valid():
            task = todos_form.save()
            return redirect("todo_list_detail", task.list.id)
    else:
        todos_form = TodoTaskForm(instance=taskupdate)
    context = {
        "todos_form": todos_form,
    }
    return render(request, "todos/item_update.html", context)
