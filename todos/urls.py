from django.urls import path
from todos.views import todo_list, todo_items, create_view, todo_update_view
from todos.views import todo_delete, task_view, task_update

urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", todo_items, name="todo_list_detail"),
    path("create/", create_view, name="todo_list_create"),
    path("<int:id>/edit/", todo_update_view, name="todo_list_update"),
    path("<int:id>/delete/", todo_delete, name="todo_list_delete"),
    path("items/create/", task_view, name="todo_item_create"),
    path("items/<int:id>/edit/", task_update, name="todo_item_update"),
]
